
#ifndef F_CPU
#define F_CPU 8000000UL // or whatever may be your frequency
#endif

#include <avr/io.h>
#include <util/delay.h>                // for _delay_ms()
#include <avr/interrupt.h>
#include "display.h"


#define IN 0
#define OUT 1

#define TC_TCT_MODE (1 << WGM21) | (1 << COM2A0)
#define TC_PRESCALE_8 (1 << CS21)

uint8_t hours = 0;
uint8_t minutes = 0;
uint8_t seconds = 0;

uint8_t set_time_state = 0;

void initialize() {
    // Initialize PORT B
    DDRB = (IN  << PB0) | // SW1
           (IN  << PB1) | // SW2
           (OUT << PB2) | // SP1
           (OUT << PB3) | // MOSI
           (IN  << PB4) | // MISO
           (IN  << PB5) | // SCK
           (IN  << PB6) | // XTAL 1
           (IN  << PB7);  // XTAL 2

    PORTB = (1 << PB0) | // SW1 Enable pull-up
            (1 << PB1) | // SW2 Enable Pull-up
            (0 << PB2) | // SP1 Cut voltage to speaker
            (1 << PB3) | // MOSI HiZ
            (0 << PB4) | // MISO HiZ
            (0 << PB5) | // SCK HiZ
            (0 << PB6) | // XTAL 1 HiZ
            (0 << PB7);  // XTAL 2 HiZ

    // Initialize PORT C
    DDRC = (OUT << PC0) | // LED ROW 0
           (OUT << PC1) | // LED ROW 1
           (OUT << PC2) | // LED ROW 2
           (OUT << PC3) | // LED ROW 3
           (IN  << PC4) | // Battery charging status (currently not connected)
           (OUT << PC5);  // Not connected

    PORTC = (1 << PC0) | // LED ROW 0 (Turn LEDS on)
            (1 << PC1) | // LED ROW 1 (Turn LEDS on)
            (1 << PC2) | // LED ROW 2 (Turn LEDS on)
            (1 << PC3) | // LED ROW 3 (Turn LEDS on)
            (0 << PC4) | // Battery charging status (currently not connected)
            (0 << PC5);  // Not connected, So just turn it off

    // Initialize PORT D
    DDRD = (OUT << PD0) | // RX
           (OUT << PD1) | // TX
           (OUT << PD2) | // LED COL ADDR 0
           (OUT << PD3) | // LED COL ADDR 1
           (OUT << PD4) | // LED COL ADDR 2
           (OUT << PD5) | // LED PWM (ALL LED ENABLE)
           (IN  << PD6) | // USB sense (Currently not connected)
           (IN  << PD7);  // SW3

    PORTD = (0 << PD0) | // RX
            (0 << PD1) | // TX
            (0 << PD2) | // LED COL ADDR 0
            (0 << PD3) | // LED COL ADDR 1
            (0 << PD4) | // LED COL ADDR 2
            (1 << PD5) | // LED PWM (ALL LED ENABLE)
            (0 << PD6) | // USB sense (Currently not connected)
            (1 << PD7);  // SW3 Enable Pull-up

}

void initialize_timer2() {
    //Disable timer2 interrupts
    TIMSK2  = 0;
    //Enable asynchronous mode
    ASSR  = (1<<AS2);
    //set initial counter value
    TCNT2 = 0;
    //set prescaller 128
    TCCR2B |= (1<<CS22) | (1<<CS20);
    //wait for registers update
    while (ASSR & ((1<<TCN2UB)|(1<<TCR2BUB)));
    //clear interrupt flags
    TIFR2  = (1<<TOV2);
    //enable TOV2 interrupt
    TIMSK2  = (1<<TOIE2);
}

void initialize_timer1() {
    // Initialize timer2 to normal mode, with prescale 8
    // Disable interrupts
    TIMSK1 = 0;
    /*OCR1B = 499; // 1kHz*/
    OCR1A = 499;
    /*OCR1A = 0x0fff;*/

    // Set initial counter value
    TCNT1 = 0;

    // Set Normal node and prescaler 8
    // TCCR1A = 0;
    // TCCR1B = (1 << CS11);

    // Set CTC mode prescale 8
    TCCR1A = (1<<COM1B0);
    TCCR1B = (1<<WGM12) | (1<<CS10);

    // clear interrupt flag
    /*TIFR1 = (1<<TOV1) | (1<<OCF1A);*/
    // Enable TOV1 interrupt
    /*TIMSK1 = (1 << TOIE1) | (1<<OCIE1A);*/
}

int main(void)
{
    display_init();
    /*initialize_timer1();*/
    initialize_timer2();
    initialize();


    // Enable interrupts
    sei();
    while(1)
    {
            /*PORTC ^= (1 << PC0) | (1 << PC1) | (1 << PC2) | (1 << PC3);*/
	/*_delay_ms(1000);*/
    }
}

#define SW1 (0x0)
#define SW2 (0x1)
#define SW3 (0x2)

#define SW1_MASK (0x1)
#define SW2_MASK (0x2)
#define SW3_MASK (0x4)

static uint8_t btn_status = 0;

void on_btn_down(uint8_t btn) {

    /*PORTC ^= (1 << btn);*/
    /*TCCR1B = (1 << WGM12) | (1 << CS10);*/
    switch(btn) {
        case SW2:
            set_time_state = (set_time_state + 1) % 2;
            break;

        case SW1:
            if(set_time_state == 0)
                hours++;
            else
                minutes++;
            break;

        case SW3:
            if(set_time_state == 0)
                hours--;
            else
                minutes--;
            break;
    }
}

void on_btn_up(uint8_t btn) {
    
    /*PORTC ^= (1 << btn);*/
    /*TCCR1B = 0; */
}


uint8_t reverse(uint8_t b) {
    b = (b & 0b1100) >> 2 | (b & 0b0011) << 2;
    b = (b & 0b1010) >> 1 | (b & 0b0101) << 1;
   return b;
}

//Overflow ISR
ISR(TIMER2_OVF_vect)
{
    uint8_t btn = PINB;
    uint8_t mod = 0;

    uint8_t new_btn_status = 0;
    new_btn_status = (PINB & (1 << PB0)) << SW1;
    new_btn_status |= ((PINB & (1 << PB1)) >> PB1) << SW2;
    new_btn_status |= ((PIND & (1 << PD7)) >> PD7) << SW3;

    uint8_t diff = btn_status ^ new_btn_status;

    if(diff & SW1_MASK) {
        if (new_btn_status & SW1_MASK)
            on_btn_up(SW1);
        else
            on_btn_down(SW1);
    }

    if(diff & SW2_MASK) {
        if (new_btn_status & SW2_MASK)
            on_btn_up(SW2);
        else
            on_btn_down(SW2);
    }

    if(diff & SW3_MASK) {
        if (new_btn_status & SW3_MASK)
            on_btn_up(SW3);
        else
            on_btn_down(SW3);
    }

    btn_status = new_btn_status;

    seconds++;
    if (seconds >= 60) {
        seconds = 0;
        minutes++;
    }
    if (minutes >= 60) {
        minutes = 0;
        hours = hours++;
    }
    if(hours >= 24) {
        hours = 0;
    }

    char data[6] = {0};
    data[0] = reverse(hours / 10);
    data[1] = reverse(hours % 10); 
    data[2] = reverse(minutes / 10);
    data[3] = reverse(minutes % 10); 
    data[4] = reverse(seconds / 10);
    data[5] = reverse(seconds % 10); 
    /*data[0] = time;*/
    display_set(data);


    asm volatile("nop"::);
    //_delay_us(10);
}

/*ISR(TIMER2_COMPA_vect) {

}*/

/*ISR(TIMER1_OVF_vect) {
    PINC = 1<<PC2;
    asm volatile("nop"::);
}*/

ISR(TIMER1_COMPA_vect) {
    /*
    PINB = (1<<PB2);
    PINC = 1<<PC3;*/
    asm volatile("nop"::);
}

ISR(BADISR_vect) {

}

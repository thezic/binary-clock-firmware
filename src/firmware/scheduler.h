#ifndef SCHEDULER_H
#define SCHEDULER_H

#include <stdint.h>

#define ERROR -1

typedef void (*TASK)();

int register_periodic_task(uint32_t period, TASK fn);
// void remove_task(id);

void run_schedule();
void tick();

#endif
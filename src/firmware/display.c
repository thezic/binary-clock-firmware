#include <avr/io.h>
#include <string.h>
#include <avr/interrupt.h>
#include "display.h"

#define LED_COLUMN_ADDR_MASK (0x7 << 2)
#define LED_COLUMN_DATA_MASK (0xf)

uint8_t display_col_update = 0;
char col = 0;
char data[6] = {0x1, 2, 4, 0x8, 0x3, 0x7};

void out_led_column(char column, char data) {
    char port_c;
    char port_d;

    PORTC |= LED_COLUMN_DATA_MASK; // Turn off all leds for a moment
    /*PORTD &= ~(1<<PD5);*/
    /*PORTC &= ~LED_COLUMN_DATA_MASK;*/
    /*PORTD &= ~(LED_COLUMN_ADDR_MASK);*/
    /*PORTD |= LED_COLUMN_ADDR_MASK;*/


    
    /*for (uint8_t i=0; i<50; ++i)*/
        /*asm volatile("nop"::);*/


    port_c = PORTC & ~(LED_COLUMN_DATA_MASK); // Reset column data, but save others
    port_d = PORTD & ~(LED_COLUMN_ADDR_MASK); // Reset LED COLUMN bits, but save the others.

    port_c |= (~data & LED_COLUMN_DATA_MASK); // Set column data
    port_d |= (column & 7) << 2;       // Set the new COLUMN bits

    PORTD = port_d; // Update column
    PORTC = port_c; // Update port c 

    /*PORTD |= (1<<PD5);*/
    
}

void display_init() {
    // Disable interrupts
    TIMSK0 = 0;
    OCR0B = 0xf9; // Set 100% duty cycle
    TCNT0 = 0; // Set initial value

    // Set timer0 to phase correct PWM, prescaler=1
    TCCR0A = (1<<WGM00) | (1<<COM0B1) | (1<<COM0B0);
    TCCR0B = (1<<CS00);

    // Clear interrupt flags
    TIFR0 = (1<<TOV0);
    // Enable TOV irq
    TIMSK0 = (1<<TOIE0);
}

void display_set(const char *new_data) {
    memcpy(data, new_data, 6);
}

void display_task() {
}

ISR(TIMER0_OVF_vect) {

    if(0 == display_col_update) {
        out_led_column(col, data[col]);
        col = (col + 1) % 6;
    }

    /*display_col_update = (display_col_update + 1) % 2;*/
}

#include <stdint.h>
#include "clock.h"
#include "display.h"

uint8_t hours = 0;
uint8_t minutes = 0;
uint8_t seconds = 0;

void clock_init() {

}

#define TURN_AROUND(X) (X)
void update_display(uint8_t h, uint8_t m, uint8_t s) {
    char data[6] = {0};
    data[0] = TURN_AROUND((h >> 4) & 0x0f);
    data[1] = TURN_AROUND(h & 0x0f);
    data[2] = TURN_AROUND((m >> 4) & 0x0f);
    data[3] = TURN_AROUND(m & 0x0f);
    data[4] = TURN_AROUND((s >> 4) & 0x0f);
    data[5] = TURN_AROUND(s & 0x0f);

    display_set(data);
}

void clock_task() {
    // increase 1s
    seconds++;

    if(seconds == 60) {
        seconds = 0;
        minutes++;
    }

    if(minutes == 60) {
        minutes = 0;
        hours++;
    }

    if(hours == 24) {
        hours = 0;
    }

    update_display(hours, minutes, seconds);
}
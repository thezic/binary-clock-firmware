#ifndef F_CPU
#define F_CPU 1000000UL // or whatever may be your frequency
#endif

#include <avr/io.h>
#include <util/delay.h>                // for _delay_ms()
#include <avr/interrupt.h>

#include "scheduler.h"
#include "display.h"
#include "clock.h"

#define IN 0
#define OUT 1

#define TC_TCT_MODE (1 << WGM21) | (1 << COM2A0)
#define TC_PRESCALE_8 (1 << CS21)

void initialize() {
    // Initialize PORT B
    DDRB = (IN  << PB0) | // SW1
           (IN  << PB1) | // SW2
           (OUT << PB2) | // SP1
           (OUT << PB3) | // MOSI
           (IN  << PB4) | // MISO
           (IN  << PB5) | // SCK
           (IN  << PB6) | // XTAL 1
           (IN  << PB7);  // XTAL 2

    PORTB = (1 << PB0) | // SW1 Enable pull-up
            (1 << PB1) | // SW2 Enable Pull-up
            (1 << PB2) | // SP1 Cut voltage to speaker
            (1 << PB3) | // MOSI HiZ
            (0 << PB4) | // MISO HiZ
            (0 << PB5) | // SCK HiZ
            (0 << PB6) | // XTAL 1 HiZ
            (0 << PB7);  // XTAL 2 HiZ

    // Initialize PORT C
    DDRC = (OUT << PC0) | // LED ROW 0
           (OUT << PC1) | // LED ROW 1
           (OUT << PC2) | // LED ROW 2
           (OUT << PC3) | // LED ROW 3
           (IN  << PC4) | // Battery charging status (currently not connected)
           (OUT << PC5);  // Not connected

    PORTC = (0 << PC0) | // LED ROW 0 (Turn LEDS on)
            (0 << PC1) | // LED ROW 1 (Turn LEDS on)
            (0 << PC2) | // LED ROW 2 (Turn LEDS on)
            (0 << PC3) | // LED ROW 3 (Turn LEDS on)
            (0 << PC4) | // Battery charging status (currently not connected)
            (0 << PC5);  // Not connected, So just turn it off

    // Initialize PORT D
    DDRD = (OUT << PD0) | // RX
           (OUT << PD1) | // TX
           (OUT << PD2) | // LED COL ADDR 0
           (OUT << PD3) | // LED COL ADDR 1
           (OUT << PD4) | // LED COL ADDR 2
           (OUT << PD5) | // LED PWM (ALL LED ENABLE)
           (IN  << PD6) | // USB sense (Currently not connected)
           (IN  << PD7);  // SW3

    PORTD = (0 << PD0) | // RX
            (0 << PD1) | // TX
            (0 << PD2) | // LED COL ADDR 0
            (0 << PD3) | // LED COL ADDR 1
            (0 << PD4) | // LED COL ADDR 2
            (1 << PD5) | // LED PWM (ALL LED ENABLE)
            (0 << PD6) | // USB sense (Currently not connected)
            (1 << PD7);  // SW3 Enable Pull-up

}

void blink_task() {
    PORTD ^= (1 << PD0);
}

int main(void)
{
    initialize();
    // ASSR = 1 << AS2; // External clock crystal

    // Setup Timer/counter2 Compare match A for 10kHz (0.1ms)
    OCR2A = 99;
    // OCR2A = 128;

    // clk/8 prescaler, TCT mode
    TCCR2A = (1 << WGM21) | (1 << COM2A0);
    TCCR2B = (1 << WGM22) | (1 << CS21);

    // Enable Timer/counter2 compare match A interrupt
    TIMSK2 = (1 << OCIE2A);

    register_periodic_task(50, &blink_task);
    // register_periodic_task(16, &display_task);
    register_periodic_task(10000, &clock_task);

    char display_data[6] = {0};
    display_set(display_data);

    // Enable interrupts
    sei();
    while(1)
    {
        run_schedule();
        display_task();
    }
}

ISR(TIMER2_COMPA_vect) {
    tick();
}

ISR(BADISR_vect) {

}

#include "scheduler.h"
#include <stdlib.h>

#define MAX_TASKS 15

typedef struct __TASK_INFO {
    uint32_t period;
    uint32_t timeleft;
    TASK fn;
} TASK_INFO;

int task_count = 0;
int schedule_count = 0;
TASK_INFO *task_list[MAX_TASKS] = { NULL };
TASK_INFO *schedule[MAX_TASKS] = { NULL };

/**
 * Register a task
 */
int register_periodic_task(uint32_t period, TASK task) {
    if(task_count==MAX_TASKS) {
        return ERROR;
    }

    int id = task_count++;
    TASK_INFO *ti = task_list[id] = malloc(sizeof(TASK_INFO));

    ti->period = ti->timeleft = period;
    ti->fn = task;

    return id;
}


void schedule_task(TASK_INFO *ti) {
    schedule[schedule_count++] = ti;
}

/**
 * Runs scheduled tasks
 */
void run_schedule() {
    for(int i=0; i<schedule_count; ++i) {
        schedule[i]->fn();
        schedule[i] = NULL;
    }

    // Reset schedule
    schedule_count = 0;
}

/**
 * Ticks system forward and add tasks to run-queue on time.
 */
void tick() {
    TASK_INFO* ti = NULL;

    for(int i=0; i<task_count; ++i) {
        ti = task_list[i];
        ti->timeleft--;

        if(ti->timeleft == 0) {
            schedule_task(ti);
            ti->timeleft = ti->period;
        }
    }
}
/**
 * Remove a task
 */
// void remove_task(id) {

// }
#include <iostream>
#include "shader.h"

void Shader::compile(const GLchar* vertex_src, const GLchar* fragment_src) {
    GLuint vertex_shader, fragment_shader;

    // Vertex shader
    vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex_shader, 1, &vertex_src, NULL);
    glCompileShader(vertex_shader);
    check_error(vertex_shader, "VERTEX");

    // Fragment shader
    fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment_shader, 1, &fragment_src, NULL);
    glCompileShader(fragment_shader);
    check_error(fragment_shader, "FRAGMENT");

    // Program
    this->ID = glCreateProgram();
    glAttachShader(this->ID, vertex_shader);
    glAttachShader(this->ID, fragment_shader);
    glLinkProgram(this->ID);

    // Cleanup
    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);
}

void Shader::use() {
    glUseProgram(this->ID);
}

void Shader::check_error(GLuint shader, std::string type) {
    GLint success;
    GLchar log[1024];

    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);

    if(!success) {
        glGetShaderInfoLog(shader, 1024, NULL, log);
        std::cout << "ERROR : SHADER : Compile time error '" << type << "'" << std::endl;
        std::cout << log << std::endl;
        std::cout << "==============================================================";
    }
}
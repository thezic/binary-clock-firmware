#include "texture.h"

Texture2D::Texture2D()
    : width(0), height(0),
      internal_format(GL_RGB), image_format(GL_RGB),
      wrap_s(GL_REPEAT), wrap_t(GL_REPEAT),
      filter_min(GL_NEAREST), filter_mag(GL_NEAREST) {

    glGenTextures(1, &this->ID);
}

Texture2D::~Texture2D() {

}

void Texture2D::loadTexture(GLuint width, GLuint height, unsigned char* data) {
    this->width = width;
    this->height = height;

    this->bind();

    glTexImage2D(GL_TEXTURE_2D, 0, this->internal_format, width, height, 0,
                 this->image_format, GL_UNSIGNED_BYTE, data);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, this->wrap_s);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, this->wrap_t);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, this->filter_min);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, this->filter_mag);

    Texture2D::unbind();
}

void Texture2D::bind() {
    glBindTexture(GL_TEXTURE_2D, this->ID);
}

void Texture2D::unbind() {
    glBindTexture(GL_TEXTURE_2D, 0);
}
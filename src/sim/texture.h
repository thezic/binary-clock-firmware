#ifndef __TEXTURE_H__
#define __TEXTURE_H__

#include <GL/glew.h>

class Texture2D {
public:
    Texture2D();
    virtual ~Texture2D();

    void loadTexture(GLuint width, GLuint height, unsigned char* data);
    void bind();
    static void unbind();

    GLuint ID;
    GLuint width;
    GLuint height;

    GLuint internal_format;
    GLuint image_format;
    GLuint wrap_s;
    GLuint wrap_t;
    GLuint filter_min;
    GLuint filter_mag;
};

#endif
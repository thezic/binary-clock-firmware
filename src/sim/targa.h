#ifndef __TARGA_H__
#define __TARGA_H__

typedef enum {
    TGA_RGB,
    TGA_RGBA
} TGA_FORMAT;

typedef struct {
    int width;
    int height;
    TGA_FORMAT pixel_format;
    unsigned char* pixels;
} TGA_FILE_INFO, *TGA_FILE_INFO_PTR;

TGA_FILE_INFO* load_tga_image(const char* filename);

void free_tga_image(TGA_FILE_INFO_PTR tga_image);

#endif
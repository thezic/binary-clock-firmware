#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "targa.h"

typedef struct {
    uint8_t id_length;
    uint8_t color_map_type;
    uint8_t image_type;

    // Color map spec
    uint16_t color_map_origin;
    uint16_t color_map_length;
    uint8_t color_map_entry_size;

    // Image spec
    uint16_t origin_x;
    uint16_t origin_y;
    uint16_t width;
    uint16_t height;
    uint8_t bpp;
    uint8_t image_descriptor_byte;
} __attribute__((packed)) TGA_HEADER;


void read_uncompressed_pixels(FILE* fp, TGA_HEADER* header, unsigned char* pixels) {
    int bytes_per_pixel = header->bpp / 8;
    int bytes_left_to_read = header->width * header->height * bytes_per_pixel;
    // uint32_t pixel,
    //          pixel_addr,
    //          pixel_index = 0;

    // uint8_t r, g, b, a;

    fread(pixels, 1, bytes_left_to_read, fp);
}

/** 
 * Load a TGA image. Supported modes are 2
 */
TGA_FILE_INFO* load_tga_image(const char* filename) {
    printf("Loading image: %s\n", filename);
    TGA_HEADER header;
    FILE* fp = fopen(filename, "rb");

    int size_read;
    // Read header
    size_read = fread(&header, 1, sizeof(header), fp);

    printf("read %d of %d bytes\n", size_read, (int)sizeof(header));

    printf("id_length: %d\n", header.id_length);
    printf("color_map_type: %d\n", header.color_map_type);
    printf("image_type: %d\n", header.image_type);
    printf("color_map_origin: %d\n", header.color_map_origin);
    printf("color_map_length: %d\n", header.color_map_length);
    printf("color_map_entry_size: %d\n", header.color_map_entry_size);
    printf("origin_x: %d\n", header.origin_x);
    printf("origin_y: %d\n", header.origin_y);
    printf("width: %d\n", header.width);
    printf("height: %d\n", header.height);
    printf("bpp: %d\n", header.bpp);
    printf("image_descriptor_byte: %d\n", header.image_descriptor_byte);

    if(header.image_type != 2 /*&& header.image_type != 10*/) {
        printf("Unable to load tga file. unsuported type: %d\n", header.image_type);
        return NULL;
    }

    // Skip image id field
    if(header.id_length > 0) {
        fseek(fp, header.id_length, SEEK_CUR);
    }

    // skip colormap
    int colormap_size = header.color_map_length * header.color_map_entry_size / 8;
    if(colormap_size > 0) {
        fseek(fp, colormap_size, SEEK_CUR);
    }

    // read pixels
    unsigned char *pixels = (unsigned char*)malloc(header.width * header.height * header.bpp / 8);

    switch(header.image_type) {
    case 2:
        read_uncompressed_pixels(fp, &header, pixels);
        break;

    default:
        printf("Unsupported TGA file");
        free(pixels);
        return NULL;
    }

    TGA_FILE_INFO_PTR info = (TGA_FILE_INFO_PTR)malloc(sizeof(TGA_FILE_INFO));
    memset(info, 0, sizeof(TGA_FILE_INFO));

    info->width = header.width;
    info->height = header.height;
    info->pixels = pixels;

    // int x, y;
    // for(y=0; y<header.height; ++y) {
    //     for(x=0; x<header.width; ++x) {
    //         printf("%.2x ", pixels[x + y * header.width]);
    //         printf("%.2x ", pixels[x + y * header.width + 1]);
    //         printf("%.2x,    ", pixels[x + y * header.width + 2]);
    //     }
    //     printf("\n");
    // }

    fclose(fp);
    return info;
}

/**
 Free the memory allocated by a TGA_FILE_INFO structure, and it's file data
 */
void free_tga_image(TGA_FILE_INFO_PTR tga_file) {
    if(tga_file && tga_file->pixels) {
        free(tga_file->pixels);
        tga_file->pixels = NULL;
    }

    if(tga_file) {
        free(tga_file);
    }
}
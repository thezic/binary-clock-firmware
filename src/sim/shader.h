#ifndef __SHADER_H__
#define __SHADER_H__

#include <string>
#include <GL/glew.h>

class Shader {
public:
    GLuint ID;

    Shader() {};

    void compile(const GLchar* vertex_src, const GLchar* fragment_src);
    void use();

private:
    void check_error(GLuint shader, std::string type);
};

#endif
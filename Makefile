export PROJECT_DIR := $(shell pwd)
export BUILD_DIR := $(PROJECT_DIR)/build

MKDIR = mkdir -p

all: directories firmware sim

directories: $(BUILD_DIR)

sim:
	$(MAKE) -C src/sim

firmware:
	$(MAKE) -C src/firmware

$(BUILD_DIR):
	$(MKDIR) $(BUILD_DIR)

.PHONY: all sim firmware directories